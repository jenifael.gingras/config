# Script et configuration

NB: Le dépôt est sur gitlab -> https://gitlab.com/jenifael.gingras/config

## Varmillo keyboard f keys

* `echo 2 >> /sys/module/hid_apple/parameters/fnmod`

## i3status

* implémentation: py3status
* py3-cmd list --all
* py3-cmd list -f module_name
