"""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""
" Plugin configurations (plug.vim)

call plug#begin('~/.config/nvim/plugged')
" Plug (self-managed)
Plug 'junegunn/vim-plug'

" Deps of other plugins
Plug 'nvim-lua/plenary.nvim' " via Telescope, Gitsigns

" Airline
Plug 'vim-airline/vim-airline'

" ALE
Plug 'dense-analysis/ale'

" Autopairs
Plug 'windwp/nvim-autopairs'

" Bufsurf
Plug 'ton/vim-bufsurf'

" Completion
Plug 'hrsh7th/nvim-cmp'
Plug 'hrsh7th/cmp-nvim-lsp'
Plug 'hrsh7th/cmp-path'

" Commentary
Plug 'b3nj5m1n/kommentary'

" Copilot
Plug 'zbirenbaum/copilot.lua'
Plug 'https://github.com/gptlang/CopilotChat.nvim', { 'branch': 'canary' }

" DAP
Plug 'mfussenegger/nvim-dap'
Plug 'leoluz/nvim-dap-go'
Plug 'mfussenegger/nvim-dap-python'
Plug 'https://github.com/piredman/nvim-dap-dotnet'
Plug 'rcarriga/nvim-dap-ui'
Plug 'https://github.com/nvim-neotest/nvim-nio'
Plug 'theHamsta/nvim-dap-virtual-text'

" Darcula theme
Plug 'blueshirts/darcula'

" Devicons
Plug 'kyazdani42/nvim-web-devicons'

" Duo (Gitlab)
" Plug 'https://gitlab.com/gitlab-org/editor-extensions/gitlab.vim'

" Epochconverter
Plug 'https://github.com/bartek/epochconverter.nvim'

" Fugitive
Plug 'tpope/vim-fugitive'
Plug 'https://github.com/shumphrey/fugitive-gitlab.vim'

" Gitsigns
Plug 'lewis6991/gitsigns.nvim'

" Harelang
" Plug 'https://git.sr.ht/~sircmpwn/hare.vim'

" Iron (repl)
" Plug 'https://gitlab.com/jenifael.gingras/iron.nvim.git'

" Markdown preview
" do a manual `:call mkdp#util#install()` after install
Plug 'iamcco/markdown-preview.nvim', { 'do': { -> mkdp#util#install() }, 'for': ['markdown', 'vim-plug']}

" Neodev
Plug 'https://github.com/folke/neodev.nvim'

" Neotest
Plug 'antoinemadec/FixCursorHold.nvim'
Plug 'nvim-neotest/neotest'
Plug 'https://github.com/Issafalcon/neotest-dotnet'
Plug 'https://github.com/nvim-neotest/neotest-go'
Plug 'https://github.com/nvim-neotest/neotest-python'

" Neovim Lsp Config
Plug 'neovim/nvim-lspconfig'

" Pum
" Plug 'Shougo/pum.vim'

" Schema Store (json/yaml)
Plug 'b0o/schemastore.nvim'

" Snippet
Plug 'hrsh7th/vim-vsnip'

" Taboo
Plug 'gcmt/taboo.vim'

" Telescope
Plug 'nvim-telescope/telescope.nvim'
Plug 'nvim-telescope/telescope-fzf-native.nvim', { 'do': 'make' }
Plug 'https://github.com/nvim-telescope/telescope-file-browser.nvim'
Plug 'https://github.com/nvim-telescope/telescope-symbols.nvim'

" Test
Plug 'https://github.com/klen/nvim-test'

" Treesitter
Plug 'nvim-treesitter/nvim-treesitter', { 'do': ':TSUpdate'}
" Plug 'nvim-treesitter/nvim-treesitter'

" UUID
Plug 'kburdett/vim-nuuid'

" Venv selector
Plug 'https://github.com/linux-cultist/venv-selector.nvim', { 'branch': 'regexp' }

" Web Devicons
Plug 'https://github.com/nvim-tree/nvim-web-devicons'

" Workspace
Plug 'https://github.com/natecraddock/workspaces.nvim'

call plug#end()


"""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""
" General settings
syntax on
filetype plugin indent on

set formatoptions-=cro
set sessionoptions-=options
set number
set hidden
set showcmd
set wildmenu
set wildmode=longest,list
set backspace=eol,start,indent
set laststatus=2
set completeopt=menu,menuone,noselect
set nowrap
set colorcolumn=120

nnoremap <silent> <c-s> <cmd>let @/ = ""<cr>
inoremap <c-z> <c-o>zz
set splitright

set jumpoptions=stack " jumplist behave like a web browser history

"""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""
" Get rid of backup file
set nobackup
set nowb
set noswapfile


"""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""
" Indentation management
set expandtab
set smarttab
set autoindent
set smartindent
set shiftwidth=4
set tabstop=4


"""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""
" Search options
set ignorecase
set smartcase
set hlsearch
set showmatch


"""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""
" Make utf8 and unix the default
set encoding=utf8
set ffs=unix,dos,mac


"""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""
" Mapping
let mapleader = ","
noremap <leader>ev :e $HOME/.config/nvim/init.vim<cr>
noremap <leader>el :e $HOME/.config/nvim/plugins.lua<cr>
noremap <leader>rv :source $MYVIMRC<cr>
nnoremap <leader><leader>s :%s/\s+$//ge \| noh <cr>
nnoremap <leader>t :split \|resize 10 \|set wfh \|term <cr> Afish<cr><c-l>
nnoremap ]] :cnext<cr>
nnoremap [[ :cprevious<cr>

nnoremap gx :!xdg-open <cfile><cr><cr>


"""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""
" Autocommand
augroup vimrc
    au!
    " au BufRead *.yml :setf ansible
    au BufRead Jenkinsfile :setf groovy
augroup end


"""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""
" Theme
set background=dark
colorscheme darcula


"""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""
" Lua config
luafile $HOME/.config/nvim/plugins.lua

"""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""
" Python
let g:python3_host_prog = "$HOME/.pyenv/versions/nvim/bin/python"

"""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""
" JSON
command JsonPretty :%!jq '.'

"""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""
" Scratch Buffer
command NewScratch :call NewScratch()<cr>


"""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""
" Airline
let g:airline_powerline_fonts = 1


"""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""
" Kommentary
nnoremap <cr> <Plug>kommentary_line_default
vnoremap <cr> <Plug>kommentary_visual_default<c-c>


"""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""
" Fzf
" nnoremap <C-p> :Files<cr>
" nnoremap <C-e> :History<cr>


"""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""
" Taboo
set sessionoptions+=tabpages,globals


"""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""
" Netrw
let g:netrw_banner = 1
let g:netrw_liststyle = 0
let g:netrw_browse_split = 4
let g:netrw_preview_split = 0
let g:netrw_altv = 1
let g:netrw_winsize = 20


"""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""
" Rainbow
let g:rainbow_active = 1


"""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""
" Something try desperately to make vim file auto comment new line...
autocmd FileType vim setlocal formatoptions-=c formatoptions-=r formatoptions-=o


"""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""
" Autopairs
let g:AutoPairsCenterLine = 0


"""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""
" Neovim LSP

" Utilities
nnoremap <leader>lsa <cmd>lua vim.lsp.buf.code_action()<CR>
vnoremap <leader>lsa <cmd>lua vim.lsp.buf.code_action()<CR>
nnoremap <leader>lsr <cmd>lua vim.lsp.buf.rename()<CR>
nnoremap <leader>lsc <cmd>verbose set omnifunc?<CR>
nnoremap <leader>lsd <cmd>lua vim.diagnostic.reset()<CR>
nnoremap <leader>lss <cmd>Telescope lsp_dynamic_workspace_symbols<CR>

" Declaration, definition, implementation, references
nnoremap <silent> gd    <cmd>lua vim.lsp.buf.declaration()<CR>
nnoremap <silent> <c-]> <cmd>lua vim.lsp.buf.definition()<CR>
nnoremap <silent> <c-[> <cmd>Telescope lsp_incoming_calls<CR>
nnoremap <silent> <leader>lsp <cmd>lua vim.lsp.buf.peek_definition()<CR>
nnoremap <silent> gD    <cmd>lua vim.lsp.buf.implementation()<CR>

" Hover, signature, formatting
nnoremap <silent> K     <cmd>lua vim.lsp.buf.hover()<CR>
inoremap <silent> <c-h> <cmd>lua vim.lsp.buf.signature_help()<CR>
nnoremap <silent> <s-l> <cmd>lua vim.lsp.buf.format()<CR>
nnoremap <silent> <c-l> <cmd>lua vim.lsp.buf.format()<CR>

" Previous|Next Error (quickfix)
nnoremap <silent> [g <cmd>lua vim.diagnostic.goto_prev()<CR>
nnoremap <silent> ]g <cmd>lua vim.diagnostic.goto_next()<CR>

" Trigger completions
inoremap <c-space> <c-x><c-o>

" Load the omnifunc for completions
augroup lspomni
    autocmd Filetype python setlocal omnifunc=v:lua.vim.lsp.omnifunc
    autocmd Filetype rust setlocal omnifunc=v:lua.vim.lsp.omnifunc
    autocmd Filetype sh setlocal omnifunc=v:lua.vim.lsp.omnifunc
    autocmd Filetype dockerfile setlocal omnifunc=v:lua.vim.lsp.omnifunc
    autocmd Filetype go setlocal omnifunc=v:lua.vim.lsp.omnifunc
    autocmd Filetype json setlocal omnifunc=v:lua.vim.lsp.omnifunc
    " autocmd Filetype vim setlocal omnifunc=v:lua.vim.lsp.omnifunc
    autocmd Filetype lua setlocal omnifunc=v:lua.vim.lsp.omnifunc
    autocmd Filetype yaml setlocal omnifunc=v:lua.vim.lsp.omnifunc
augroup end

augroup lspsave
    autocmd BufWritePre *.py lua vim.lsp.buf.format({ async = false })
    autocmd BufWritePre *.go lua vim.lsp.buf.format({ async = false })
    autocmd BufWritePre *.rs lua vim.lsp.buf.format({ async = false })
augroup end


"""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""
" Iron REPL
" let g:iron_map_defaults = 0
" let g:iron_map_extended = 0
" nmap <leader>rr :IronRepl<cr>
" nmap <leader>rf :IronFocus<cr>
" nmap <leader>rs <Plug>(iron-send-line)
" nmap <leader>rm <Plug>(iron-send-motion)
" nmap <leader>rl <Plug>(iron-clear)
" vmap <leader>rs <Plug>(iron-visual-send)
" nmap <leader>ri mm<leader>(<Plug>(iron-send-motion)%`m
" nmap <leader>rd mm[[<Plug>(iron-send-motion)]]`m
" nmap <leader>rcc <Plug>(iron-interrupt)


"""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""
" S-EXP
let g:sexp_enable_insert_mode_mappings = 0
nmap <leader>( <Plug>(sexp_move_to_prev_top_element)
nmap <leader>) <Plug>(sexp_move_to_next_top_element)


""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""
" Treesitter
" set foldmethod=indent
" set foldlevelstart=2
" set foldmethod=expr
" set foldexpr=nvim_treesitter#foldexpr()

""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""
" Telescope
nnoremap <c-p> <cmd>Telescope find_files<cr>
nnoremap <c-e> <cmd>Telescope oldfiles<cr>
nnoremap <leader>fg <cmd>Telescope grep_string<cr>
nnoremap <c-b> <cmd>Telescope file_browser<cr>
nnoremap <leader>rg <cmd>Telescope live_grep<cr>

" LSP binding
nnoremap <silent> gr    :Telescope lsp_references<CR>

""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""
" Zettelkasten
command ZetSearch :lua require('telescope.builtin').find_files({cwd = "~/Nextcloud/01-Documents/Zettelkasten"})
nnoremap <leader>zs <cmd>ZetSearch<cr>
nnoremap <leader>zz :call GetZetName()<cr> :Zet <c-r>=ZetName<cr><cr>


function GetZetName()
    call inputsave()
    let g:ZetName = input("zet filename: ")
    call inputrestore()
endfunction

""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""
" DAP
nnoremap <silent> <S-F1> :lua require'dap'.run_last()<cr>
nnoremap <silent> <F3> <cmd>lua require'dapui'.eval()<cr>
nnoremap <silent> <F4> :lua require'dapui'.open()<cr>
nnoremap <silent> <S-F4> :lua require'dapui'.close()<cr>
nnoremap <silent> <F5> :lua require'dap'.continue()<cr>
nnoremap <silent> <S-F5> :lua require'dap'.run_to_cursor()<cr>
nnoremap <silent> <F6> :lua require'dapui'.close()<cr>
nnoremap <silent> <F8> :lua require'dap'.toggle_breakpoint()<cr>
nnoremap <silent> <S-F8> :lua require'dap'.clear_breakpoints()<cr>
nnoremap <silent> <F9> :lua require'dap'.set_breakpoint(vim.fn.input('Breakpoint condition: '), nil, nil)<cr>
nnoremap <silent> <F10> :lua require'dap'.step_over()<cr>
nnoremap <silent> <S-F10> :lua require'dap'.step_back()<cr>
nnoremap <silent> <F11> :lua require'dap'.step_into()<cr>
nnoremap <silent> <F12> :lua require'dap'.step_out()<cr>
nnoremap <silent> <S-F12> :lua require'dap'.terminate()<cr>
augroup debugcmd
    command DebugUp lua require'dap'.up()
    command DebugDown lua require'dap'.down()
augroup end

""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""
" Copilot
" imap <silent><script><expr> <c-j> copilot#Accept("")
" let g:copilot_no_tab_map = v:true
" imap <silent><script><expr> <c-]> copilot#Suggest()
" let g:copilot_filetypes = {
"             \ '*': v:true,
"             \ 'dap-repl': v:false,
" \}

""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""
" ALE
" let g:ale_linters = {'sh': ['shellcheck'], 'python': []}
let g:ale_linters = {'sh': ['shellcheck']}
let g:ale_linters_explicit = 1

""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""
" Hyperdoc Zaxxon
command! -nargs=* Hyper call local#hyperdoc#edit(<f-args>)
command HyperSearch :lua require('telescope.builtin').find_files({cwd = "~/work/ds-platform-documentation/doc/source/hyper/"})

""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""
" Extend gf
augroup rstsuffixes
    autocmd!

    let associations = [
                \["rst", ".rst"],
                \]

    for ft in associations
        execute "autocmd FileType " . ft[0] . " setlocal suffixesadd=" . ft[1]
    endfor
augroup end

""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""
" Copilot Chat
command Chat CopilotChatToggle

""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""
" Test
" nmap <leader>yt :TestNearest<cr>
" nmap <leader>yf :TestFile<cr>
" nmap <leader>ys :TestSuite<cr>
" nmap <leader>yl :TestLast<cr>

""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""
" Markdown Preview
let g:mkdp_browser = '/usr/local/bin/qutebrowser-nix'
let g:mkdp_echo_preview_url = 1
