func! local#hyperdoc#edit(...)
    " Build the file name
    let s:sep = ''
    if len(a:000) > 0
        let s:sep = '-'
    endif

    let s:fname = expand("~/work/ds-platform-documentation/doc/source/hyper/") . join(a:000, '-') . s:sep . strftime("%Y-%m-%d") . '.rst'

    exec "edit " . fnameescape(s:fname)

    " Fill header content
    let s:note = []
    let s:human_date = strftime("%F")
    let s:title = ""

    if len(a:000) > 0
        let s:title = join(a:000)
    endif

    let s:fulltitle = s:title . " " . s:human_date
    let s:rst_markup = repeat("=", len(s:fulltitle) - 1)

    call add(s:note, s:fulltitle)
    call add(s:note, s:rst_markup)

    " Write note header content
    let s:failed = append(0, s:note)

    " Maybe some error management?!
endfunc
