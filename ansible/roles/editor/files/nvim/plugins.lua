-- General
-- local map = function(mode, lhs, rhs, opts)
--     local options = { noremap = true }
--     if opts then
--         options = vim.tbl_extend('force', options, opts)
--     end
--     vim.api.nvim_set_keymap(mode, lhs, rhs, options)
-- end

local map = vim.api.nvim_set_keymap

--------------------------------------------------------------------------------
-- Neodev
require('neodev').setup({
    setup_jsonls = false,
})

--------------------------------------------------------------------------------
--LSP client configs
--pyls
local nvim_lsp = require'lspconfig'

--nvim-cmp capabilities
local capabilities = require('cmp_nvim_lsp').default_capabilities()

local pylsp_on_attach = function(_, bufnr)
    local function buf_set_keymap(...) vim.api.nvim_buf_set_keymap(bufnr, ...) end
    -- local function buf_set_option(...) vim.api.nvim_buf_set_option(bufnr, ...) end

    local opts = { noremap=true, silent=true }

    buf_set_keymap('n', '<Leader>ca', '<cmd>lua vim.lsp.buf.code_action()<CR>', opts)
    buf_set_keymap('v', '<Leader>ca', '<cmd>lua vim.lsp.buf.range_code_action()<CR>', opts)
end

nvim_lsp.pylsp.setup{
    capabilities = capabilities,
    on_attach = pylsp_on_attach,
    settings = {
        pylsp = {
            plugins = {
                flake8 = {
                    enabled = true,
                    maxLineLength = 132,
                    perFileIgnores = {"**/*__init__.py:F401"}
                },
                pycodestyle = {
                    maxLineLength = 132
                },
                pydocstyle = {
                    enabled = true
                },
                pyflakes = {
                    enabled = false
                },
                mccabe = {
                    enabled = true,
                    threshold = 6
                },
            }
        }
    }
}

--rust analyzer
nvim_lsp.rust_analyzer.setup{
    capabilities = capabilities,
    settings = {
        ["rust-analyzer"] = {
            completion = {
                callable = {
                    snippets = "add_parentheses"
                },
            },
        }
    }
}

--bash ls
nvim_lsp.bashls.setup{
    capabilities = capabilities,
}

--dockerls
nvim_lsp.dockerls.setup{
    capabilities = capabilities,
}

--fennel_ls
nvim_lsp.fennel_ls.setup{
    capabilities = capabilities,
}

--go lsp
nvim_lsp.gopls.setup{
    capabilities = capabilities,
    root_dir = nvim_lsp.util.root_pattern("go.mod", "go.mod", ".git", "main.go#REMOVE#"),
}

--jsonls
nvim_lsp.jsonls.setup{
    capabilities = capabilities,
    settings = {
        json = {
            schemas = require('schemastore').json.schemas(),
            validate = { enable = true },
        }
    }
    -- cmd = {"/usr/bin/vscode-json-languageserver"}
}

--lua
nvim_lsp.lua_ls.setup{
    capabilities = capabilities,
    cmd = {"/usr/local/bin/lua-language-server"};
}

--yamlls
require'lspconfig'.yamlls.setup{
    capabilities = capabilities,
    settings = {
        yaml = {
            schemas = require('schemastore').yaml.schemas {
                extra = {
                    {
                        description = "Common Ecosystem Spec",
                        -- fileMatch = {"env/*.yml", "feature/*.yml"},
                        name = "ecosystem.json",
                        url = "file:///home/jenifael.gingras-cou@optelvision.local/tmp/schemas/ecosystem.json"
                    },
                },

            },
            schemaStore = {
                enable = false,
                url = "",
            }
        }
    }
}

--vimls
require'lspconfig'.vimls.setup{
    capabilities = capabilities,
}

--tsserver
require'lspconfig'.ts_ls.setup{
    capabilities = capabilities,
}

--ansiblels
require'lspconfig'.ansiblels.setup{
    capabilities = capabilities,
}

--powershell_es
require'lspconfig'.powershell_es.setup{
    capabilities = capabilities,
    bundle_path = "/opt/powershell-editor-service",
    -- cmd = {'pwsh', '-NoLogo', '-Command', "/opt/powershell-editor-service/PowerShellEditorServices/Start-EditorServices.ps1"}
}

--csharp_ls
require'lspconfig'.csharp_ls.setup({})

--harelang
--nvim_lsp.hare.setup{}

--gitlab
--[[ require('gitlab').lsp.server.setup{
    'git@gitlab.com:gitlab-org/editor-extensions/gitlab.vim.git',
    event = { 'BufReadPre', 'BufNewFile' },
    ft = { 'go', 'javascript', 'python', 'ruby' },
    cond = function()
        return vim.env.GITLAB_TOKEN ~= nil and vim.env.GITLAB_TOKEN ~= ''
    end,
    opts = {
        statusline = {
            enabled = true,
        },
    },
} ]]

--------------------------------------------------------------------------------
-- Treesitter
require'nvim-treesitter.configs'.setup {
    ensure_installed = {"python", "go", "yaml", "json", "html", "c_sharp"},
    sync_install = true,

    highlight = {
        enable = true,
        additional_vim_regex_highlight = false,
    },
}

--------------------------------------------------------------------------------
-- Telescope
require('telescope').setup {
    defaults = {
        layout_strategy = 'vertical',
        layout_config = {
            height = 0.9,
            width = 0.9,
            prompt_position = "bottom",
            preview_cutoff = 120,
        },
    },
    pickers = {
        lsp_references = {
            show_line = false,
            theme = "dropdown"
        }
    },
    extensions = {
        fzf = {
            fuzzy = true,
            override_generic_sorter = true,
            override_file_sorter = true,
            case_mode = "smart_case",
        },
        file_browser = {
            depth = false,
            files = false,
        }
    }
}
local telescope = require('telescope')
telescope.load_extension('fzf')
telescope.load_extension('file_browser')
telescope.load_extension("workspaces")
map('i', '<c-l>', '<c-o>:Telescope symbols<cr>', {noremap=false})

--------------------------------------------------------------------------------
-- DAP

-- CSharp
require('nvim-dap-dotnet').setup({})

-- Go (delve)
require('dap-go').setup()

-- Python (debugpy)
require('dap-python').setup('python')
require('dap-python').test_runner = 'pytest'

-- Virtual text
require('nvim-dap-virtual-text').setup()

-- UI
require("dapui").setup({
    icons = { expanded = "▾", collapsed = "▸" },
    mappings = {
        -- Use a table to apply multiple mappings
        expand = { "<CR>", "<2-LeftMouse>" },
        open = "o",
        remove = "d",
        edit = "e",
        repl = "r",
    },
    layouts = {
        {
            elements = {
                -- Provide as ID strings or tables with "id" and "size" keys
                {
                    id = "scopes",
                    size = 0.25, -- Can be float or integer > 1
                },
                { id = "breakpoints", size = 0.25 },
                { id = "stacks", size = 0.25 },
                { id = "watches", size = 00.25 },
            },
            size = 40,
            position = "right", -- Can be "left", "right", "top", "bottom"
        },
        {
            elements = { "repl" },
            size = 10,
            position = "bottom", -- Can be "left", "right", "top", "bottom"
        },
    },
    floating = {
        max_height = nil, -- These can be integers or a float between 0 and 1.
        max_width = nil, -- Floats will be treated as percentage of your screen.
        border = "single", -- Border style. Can be "single", "double" or "rounded"
        mappings = {
            close = { "q", "<Esc>" },
        },
    },
    windows = { indent = 1 },
})

-- Completion
local cmp = require'cmp'

cmp.setup({
    snippet = {
        expand = function(args)
            vim.fn["vsnip#anonymous"](args.body)
        end,
    },
    mapping = cmp.mapping.preset.insert({
        ['<C-n>'] = cmp.mapping.select_next_item(),
        ['<C-p>'] = cmp.mapping.select_prev_item(),
        ['<C-e>'] = cmp.mapping.confirm({ select = true }),
    }),
    sources = cmp.config.sources({
        { name = 'nvim_lsp' },
        { name = 'path' },
    }),
    cmp.setup.filetype('markdown', {
        sources = cmp.config.sources({
            {
                name = 'zettelkasten',
                option = {
                    zettelkasten_path = vim.env['HOME'] .. "/Nextcloud/01-Documents/Zettelkasten"
                },
            },
        }),
    }),
    cmp.setup.filetype('rst', {
        sources = cmp.config.sources({
            {
                name = 'hyperdoc',
                option = {
                    zettelkasten_path = vim.env['HOME'] .. "/work/ds-platform-documentation/doc/source/hyper"
                }
            },
        }),
    }),
})

-- Kommentary
require('kommentary.config').configure_language("default", {
    prefer_single_line_comments = true,
})
require('kommentary.config').configure_language("fish", {
    single_line_comment_string = "#",
})

-- Autopairs
require('nvim-autopairs').setup()
local cmp_autopairs = require('nvim-autopairs.completion.cmp')
cmp.event:on('confirm_done', cmp_autopairs.on_confirm_done())

-- Copilot
require('copilot').setup({
    panel = {
        enabled = false,
        auto_refresh = false,
        keymap = {},
        layouts = {},
    },
    suggestion = {
        enabled = true,
        auto_trigger = true,
        debounce = 75,
        keymap = {
            accept = "<C-j>",
            accept_word = false,
            accept_line = false,
            next = "<M-]>",
            prev = "<M-[>",
            dismiss = "<C-]>",
        },
    },
    filetypes = {
        ["*"] = true,
        ["dap-repl"] = false,
    },
})

-- Test
require('nvim-test').setup{
    termOpts = {
        direction = 'float',
        width = 96,
        height = 24,
    }
}

map("n", "<Leader>yt", ":TestNearest<CR>", {})
map("n", "<Leader>yf", ":TestFile<CR>", {})
map("n", "<Leader>ys", ":TestSuite<CR>", {})
map("n", "<Leader>yl", ":TestLast<CR>", {})
-- vim.api.nvim_set_keymap("n", "<Leader>yt", ":TestNearest<CR>", {noremap = true, silent = true})

--------------------------------------------------------------------------------
-- Web Devicons
require('nvim-web-devicons').setup()

--------------------------------------------------------------------------------
-- Workspace config
local workspaces = require('workspaces')

local open_pre_hook = function()
    -- at start there is a single tab with no name (nil)
    -- in this case we must skip opening a new tab
    -- otherwise we need to do a TabooOpen

    local current_tab_name = vim.t.taboo_tab_name
    local current_tab_number = vim.fn.tabpagenr('$')
    if not current_tab_name and current_tab_number == 1 then
        return
    end

    vim.cmd("TabooOpen fubar")
end

local open_hook = function()
    print("workspace " .. workspaces.name() .. " loaded")
    vim.cmd("TabooRename " .. workspaces.name())
end

workspaces.setup({
    cd_type = "tab",
    auto_open = true,
    hooks = {
        open_pre = {open_pre_hook},
        open = {open_hook},
    }
})
map('n', '<leader>w', ':Telescope workspaces<cr>', {noremap=false})

--------------------------------------------------------------------------------
-- DAP Config
local dap_execute = function()
    local dap = require('dap')
    local expr = vim.fn.input("Dlv expression: ")
    dap.execute(expr)
end
vim.keymap.set('n', '<leader>de', dap_execute)

--------------------------------------------------------------------------------
-- CopilotChat
require('CopilotChat').setup({
    debug = false,
    -- model = 'gpt-4',
    -- temperature = '0.1',
})


--------------------------------------------------------------------------------
-- Neotest
require("neotest").setup({
    adapters = {
        require("neotest-dotnet")({
            dap = {
                adapter_name = "coreclr",
            },
            discovery_root = "project",
        }),
        require("neotest-go")({
            experimental = {
                test_table = true,
            },
            args = {
                "-count=1",
                "-timeout=60s",
            },
        }),
    }
})
vim.keymap.set('n', '<leader>ns', ':lua require("neotest").summary.toggle()<cr>')
vim.keymap.set('n', '<leader>nn', ':lua require("neotest").run.run()<cr>')
vim.keymap.set('n', '<leader>nf', ':lua require("neotest").run.run(vim.fn.expand("%"))<cr>')
vim.keymap.set('n', '<leader>nd', ':lua require("neotest").run.run({strategy = "dap"})<cr>')
vim.keymap.set('n', '<leader>nu', ':lua require("neotest").run.stop()<cr>')
vim.keymap.set('n', '<leader>nl', ':lua require("neotest").run.run_last()<cr>')
vim.keymap.set('n', '<leader>nk', ':lua require("neotest").run.run_last({strategy = "dap"})<cr>')

--------------------------------------------------------------------------------
-- Epochconverter


--------------------------------------------------------------------------------
-- Venv-Selector
require('venv-selector').setup({
    settings = {
        options = {
            debug = false,
            require_lsp_activation = false,
        }
    }
})

local venv_selection = function()
    -- check if in the cwd there is a .venv folder and if so issue a venv-select
    local venv_path = vim.fn.getcwd() .. "/.venv"
    if not vim.fn.isdirectory(venv_path) then
        return
    end

    local python_path = venv_path .. "/bin/python"
    require('venv-selector').activate_from_path(python_path)
end

local venv_selector_autocmd_group = vim.api.nvim_create_augroup("VenvSelector", { clear = true })
vim.api.nvim_create_autocmd("DirChanged", {
    pattern = "tabpage",
    callback = venv_selection,
    group = venv_selector_autocmd_group,
})

--------------------------------------------------------------------------------
-- C# Config
-- add an auto command to trigger lsp format on save
vim.api.nvim_create_autocmd("BufWritePre", {
    pattern = "*.cs",
    callback = function()
        vim.lsp.buf.format()
    end,
})
