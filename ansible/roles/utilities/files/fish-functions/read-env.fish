function read-env --argument-names 'filename' --description 'read a file with key=value on each line'
    export (grep "^[^#]" "$filename" |xargs -L 1)
end
