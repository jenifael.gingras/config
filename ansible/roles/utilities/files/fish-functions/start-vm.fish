#!/bin/env fish
function start-vm
    argparse --name=start-vm 'h/help' 'n/name=' -- $argv

    set vm_listing (gcloud compute instances list --filter "name~$_flag_name-[\d]+" --format 'value(NAME, EXTERNAL_IP)')
    echo "Starting VMs"
    for vm in $vm_listing
        set vm_split (string split \t $vm)
        gcloud compute instances start "$vm_split[1]"
    end

    set vm_listing (gcloud compute instances list --filter "name~$_flag_name-[\d]+" --format 'value(NAME, EXTERNAL_IP)')
    echo "Rewriting /etc/hosts"
    for vm in $vm_listing
        set vm_split (string split \t $vm)
        set rep "$vm_split[2]\t$vm_split[1]"
        sed -n "/$vm_split[1]/q42" /etc/hosts
        if test $status -ne 42
            echo "Line absent: appending"
            echo -e $rep | sudo tee -a /etc/hosts
        else
            echo "Line modified"
            sudo sed -i "/$vm_split[1]/c$vm_split[2]\t$vm_split[1]" /etc/hosts
        end
    end
end
