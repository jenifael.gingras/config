#!/bin/env fish
function stop-vm
    gcloud compute instances stop $argv
end
