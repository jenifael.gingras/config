#!/bin/env fish
function podman-drop -d "Stop and remove a container" -a name
    # check if container exists
    if not podman container exists $name
        return
    end
    podman stop $name > /dev/null
    podman container rm $name > /dev/null
end
