set -x DJANGO_SETTINGS_MODULE optel_osm_application.settings.dev
set -x OPTELBRIDGE_DEPLOYER_GENERAL_PATH "/mnt/general/"
set -x OPTELBRIDGE_HOME "/opt/optelbridge"
set -x BRIDGE_LOGS_PATH "/var/log/optelbridge"
set -x BRIDGE_SERIAL_BACKUP_LOCATION "/opt/optelbridge"

set -gx PATH $PATH $HOME/.deno/bin
set -gx PATH $PATH $HOME/go/bin
set -gx GOPRIVATE gitlab.com
set -gx CUCINA_GITLAB_TOKEN $GITLAB_TOKEN
set -gx LUA_PATH "/opt/luarocks-3.9.2/share/lua/5.4/?.lua"
set -gx LUA_CPATH "/opt/luarocks-3.9.2/lib/lua/5.4/?.so"
set -gx BROWSER "qutebrowser"
set -gx PSModulePath "$HOME/work/mono/scripts"
set -gx PULUMI_CONFIG_PASSPHRASE_FILE "$HOME/.pulumi/passphrase"
set -gx DSP_SOFTWARE_RELEASE_DRIVE_ID "0AGrjZodoAQHqUk9PVA"
set -gx CUCINA_PROJECT_ID "51582225"

alias fd="fdfind"

set -gx DOTNET_ROOT "/opt/dotnet-sdk-8.0.303-linux-x64"
set -gx PATH $PATH $DOTNET_ROOT
set -gx PATH $PATH $DOTNET_ROOT/tools
set -gx PATH $PATH $HOME/.dotnet/tools
set -gx DOTNET_CLI_TELEMETRY_OPTOUT 1
set -gx DOTNET_UPGRADEASSISTANT_TELEMETRY_OPTOUT 1

set -gx OPTEL_NPM_REGISTRY_TOKEN $GITLAB_NPM_REGISTRY_TOKEN
