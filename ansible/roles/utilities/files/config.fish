set -x fish_key_bindings fish_vi_key_bindings
set -x BROWSER qutebrowser
set -x EDITOR nvim
set -x FZF_DEFAULT_COMMAND 'rg --hidden -l ""'
set -x _JAVA_AWT_WM_NONREPARENTING 1
set -x TERM screen-256color

set -x OPENCV_LOG_LEVEL ERROR

set -x QT_QPA_PLATFORM wayland-egl
# set -x QT_QPA_PLATFORM xcb

alias tts-fr "rlwrap espeak-ng -v mb/mb-fr4 -s 120"
alias tts-en "rlwrap espeak-ng -v mb/mb-us1 -s 140"

pyenv init --path | source
pyenv init - | source

set -x SSH_AUTH_SOCK /run/user/(id -u)/ssh-agent.socket

if test -e $HOME/.config/fish/token.fish
    # echo "Loading tokens"
    source "$HOME/.config/fish/token.fish"
end

if test -e $HOME/.config/fish/$hostname.fish
    # echo "Loading host specific fish config"
    source "$HOME/.config/fish/$hostname.fish"
end

set -x BOLT_DISABLE_ANALYTICS true

# Fix varmillo keyboard, visudo allow passwordless for this exact command (tee ...)
echo 2 | sudo tee /sys/module/hid_apple/parameters/fnmode 2&> /dev/null

set -x VAGRANT_DEFAULT_PROVIDER libvirt
set -x PATH $PATH $HOME/.cargo/bin

alias git-magic-log "git log --graph --decorate --pretty=oneline --abbrev-commit --all"
alias go120 "/opt/go-1.20.2/bin/go"
alias go121 "/opt/go-1.21.1/bin/go"
alias dtnow "date +%Y%m%d%H%M"

function todt
    date -d @$argv[1]
end
