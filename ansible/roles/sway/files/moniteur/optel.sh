#!/usr/bin/bash
swaymsg 'output * disable'
swaymsg 'output eDP-1 res 1920x1080 pos 0 0 enable scale 1.0'
swaymsg 'output DP-1 res 1920x1080 pos 1920 0 enable transform 0'
swaymsg 'output DP-3 res 1920x1080 pos 3840 0 enable transform 270'
