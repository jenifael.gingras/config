#!/usr/bin/env python3
# This script take as the first argument the path to zipfile containing one or more pay statements.
# This script extract the zipfile, rename in a std way the pdf and move them to the right folder.

PAY_STATEMENT_DIRPATH = "/home/jenifael.gingras-cou@optelvision.local/Nextcloud/01-Documents/Optel/talon/maptrack/"
# std filename format: YYYY-MM-DD.pdf
# zip file name format: "Pay Date YYYY-MM-DD.zip"
# do the script correctly, create a main method, use argparse to get arguments and give a meaningful help message
# the script is directly executable so add the relevant code
# use stdlib pathlib to simplify path management
import os

def main():
    check_preconditions()
    import argparse
    import zipfile
    from pathlib import Path

    parser = argparse.ArgumentParser(description="Extract pay statements from a zip file and move them to the right folder.")
    parser.add_argument("zipfile", help="Path to the zipfile containing the pay statements.")
    args = parser.parse_args()

    zippath = Path(args.zipfile)
    if not zippath.exists():
        print(f"Error: {zippath} does not exist.")
        return

    with zipfile.ZipFile(zippath, "r") as zipf:
        handle_pay_statement(zipf)

def check_preconditions():
    """ Should only be run with the working directory as: ~/Téléchargements (or ~/Downloads)"""
    cwd = os.getcwd()
    if not cwd.endswith("Téléchargements") and not cwd.endswith("Downloads"):
        print("Error: This script should be run from the Downloads directory.")
        exit(1)

def handle_pay_statement(zipf):
    for name in zipf.namelist():
        if not name.endswith(".pdf"):
            continue

        date = name.split()[2]
        # there is a bug with newname where the extension .pdf is added twice, fix it
        newname = f"{date}.pdf"
        newname = newname[:-4]
        zipf.extract(name, path=PAY_STATEMENT_DIRPATH)
        # check if the file already exists and if so, print an error and skip the file
        if os.path.exists(os.path.join(PAY_STATEMENT_DIRPATH, newname)):
            print(f"Error: {newname} already exists.")
            continue
        os.rename(os.path.join(PAY_STATEMENT_DIRPATH, name), os.path.join(PAY_STATEMENT_DIRPATH, newname))

if __name__ == "__main__":
    main()
